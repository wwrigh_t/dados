|   |   |
|---|---|
|Fonte | [Lista de feriados municipais]|
| Metodologia| Todos os concelhos portugueses e o respetivo santo popular que celebram. Concelhos com campo "blank" não celebram nenhum santo popular. |
| Artigo(s)| [Interativo. O país dos Santos Populares](http://rr.sapo.pt/el/116656/interativo-o-pais-dos-santos-populares)|
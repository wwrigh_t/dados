|   |   |
|---|---|
|Fonte | [sofifa](https://sofifa.com/)|
| Metodologia| Dados de todos os jogadores que foram convocados pelas seleções de Portugal e do Uruguai no popular videojogo FIFA'18. Os dados foram recolhidos sem auxílio a um scraper.|
| Artigo(s)| [Interactivo. E se o Uruguai - Portugal fosse uma partida de FIFA?](http://rr.sapo.pt/multimedia/117276/interativo-e-se-o-uruguai-portugal-fosse-uma-partida-de-fifa)|
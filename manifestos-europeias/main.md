---
title: "O que é que dizem os 
programas dos partidos para as europeias?"
author: "Rui Barros (rui.barros@rr.pt)"
date: "2019-05-20"
output:
   html_document:
     keep_md: TRUE
     code_folding: show
     echo: TRUE
     warning: FALSE
     message: FALSE
     theme: lumen
     df_print: kable
     toc: yes
     toc_depth: 4
     toc_float: 
       collapsed: false
       smooth_scroll: false
subtitle: Confia em mim, mas não me leias...
---

# Análise aos Manifestos Eleitorais

A Renascença procedeu a uma análise textual de todos os programas eleitorais dos partidos candidatos às Eleições Europeias.

Começamos por procurar todos os manifestos eleitorais de todos os partidos.

- [Aliança](input/pdf/alianca.pdf)
- [BASTA](input/basta.txt)
- [Bloco de Esquerda](input/pdf/bloco.pdf)
- [CDS](input/cds.pdf)
- [CDU(Declaração Programática do PCP + 10 Compromissos do Partido Ecologista «Os Verdes»)](input/cdu.txt)
- [Iniciativa Liberal](input/pdf/iniciativa_liberal.pdf)
- [Livre](input/pdf/livre.pdf)
- [MAS](input/mas.txt)
- [Nós Cidadãos](input/nos_cidadadaos.txt)
- [PAN](input/pdf/PAN.pdf)
- [PCTP/MRPP](input/pdf/PCTP_MRPP.pdf)
- [PDR](input/pdr.txt)
- [PNR](input/pnr.txt)
- [PS](input/ps.txt)
- [PSD](input/pdf/psd.pdf)
- [PTP](input/ptp.txt)
- [PURP](input/pdf/purp.pdf)

A menos de duas semanas das europeias, dois partidos/coligações não disponibilizavam no seu site os seus manifestos/programas eleitorais: o Basta! e o PTP. Contactatados pela **Renascença** estes partidos disponibilizaram os seus programas.

No caso da CDU, a coligação apresenta dois documentos: a [Declaração Programática do PCP](https://www.cdu.pt/parlamentoeuropeu2019/declaracao-programatica-do-pcp) e [10 Compromissos do Partido Ecologista «Os Verdes»](https://www.cdu.pt/parlamentoeuropeu2019/10-compromissos-do-partido-ecologista-os-verdes). Nesse caso, a **Renascença** considerou os dois documentos como sendo um só.

## Dependências

Para a análise, usamos as seguintes pacotes de R:


```r
library("needs")
needs(pdftools,dplyr,purrr,readr,tidyr,tidytext,stringr,rio,knitr,stopwords,ggplot2,wordcloud,tibble,quanteda)
```

# Importação de dados

Alguns documentos estão disponíveis no site dos partidos/coligações, pelo que nesses casos a **Renascença** apenas procedeu ao copy/paste dos dados para arquivos .txt. Noutros casos, os partidos disponibilizaram os seus programas em ficheiros editáveis como .docx, pelo que a **Renascença** apenas procedeu à sua transformação em ficheiros .txt.

Nos caso em que o programa estava em .pdf, foi necessário proceder ao processamento desse documento para .txt.

## Converter programas em pdf em txt

Para converter os ficheiros .pdf, recorreu-se ao package *pdftools*, que converetu os ficheiros pdf em ficheiros .txt.


```r
x <- list.files(path = "input/pdf")

for (i in 1:9) {
   text <- pdf_text(paste0("input/pdf/", x[i])) %>% 
         strsplit("\n") %>% 
         lapply(write, paste0("output/", x[i],".txt"), append=TRUE)
}
```

```
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
## PDF error: Invalid Font Weight
```

Depois do prcessamento dos .pdfs, foi necessário proceder à necessária limpeza dos ficheiros. Optou-se por preservar o máximo possível a integridade dos textos, tendo-se apenas eliminado espaços desncessáriamente acrescentados na conversão, bem como o texto referente ao número de página, cabeçalhos e rodapés.

Esta limpeza foi feita de forma manual, tendo-se verificado todos os ficheiros individualmente. Quando os documentos estavam suficientemente limpos para serem utilizados na posterior análise, foram movidos para [input/manifestos_clean](input/manifestos_clean/).

## Unnest de todos os programas políticos

Foram assim importados todos os ficheiros na pasta [input/manifestos_clean](input/manifestos_clean/).



```r
tbl <- list.files(path = "input/manifestos_clean",pattern = "*.txt",
               full.names = T) 

df <- tbl %>%
       map_chr(~ read_file(.)) %>%
       data_frame(text = .,partido = tbl)
```

```
## Warning: `data_frame()` is deprecated, use `tibble()`.
## This warning is displayed once per session.
```

```r
#remove path do nome
df$partido <- gsub("input/manifestos_clean/","",df$partido)
```

Procedeu-se à consequente tokenização destas palavras:

```r
data <- df %>%
         unnest_tokens(word, text)
nrow(data)
```

```
## [1] 93216
```

Foram assim tidas, como ponto de partida, as 93.216 palavras que compõe os programas eleitorais das 18 forças políticas.

No entanto, nem todas as palavras interessam na análise textual uma vez que há palavras como os determinantes e afins que, apesar de muito comuns no uso corrente da linguagem, não acrescentam muito - as chamadas Stop Words.

Para esta análise, consideraram-se as Stop Words para Língua Portuguesa elencadas pelo package [Stopwords](http://stopwords.quanteda.io/reference/stopwords.html)


```r
stop_words <- as.data.frame(stopwords("pt", source = "stopwords-iso"))
stop_words <- rename(stop_words, 'word' = 'stopwords("pt", source = "stopwords-iso")')
export(stop_words, "output/stop_words_pt.csv","csv")
```

As palavras eliminadas estão disponíveis [aqui](output/stop_words_pt.csv).


```r
data <- data %>% 
        anti_join(stop_words)
```

```
## Joining, by = "word"
```

```
## Warning: Column `word` joining character vector and factor, coercing into
## character vector
```

```r
nrow(data)
```

```
## [1] 46675
```
Com a eliminação das stop words, o dataset de palavras de todos os partidos corresponde assim a 46.744 palavras.



# Análise

# Quais as palavras mais usadas em todos os corpos textuais


```r
top_palavras <- data %>%
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))

export(top_palavras,"output/top_palavras_mais_usadas.csv","csv")

top_palavras %>% filter(n > 200) %>% 
                  ggplot(aes(word, n)) +
                  geom_col() +
                  xlab(NULL) +
                  coord_flip()
```

![](main_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

Em núvem de palavras...

```r
pal <- brewer.pal(8,"Dark2")

top_palavras %>% 
  with(wordcloud(word, n, random.order = FALSE, max.words = 50, colors=pal))
```

![](main_files/figure-html/unnamed-chunk-8-1.png)<!-- -->


# Qual o sentimento mais perdominante nos textos

Uma das análises possíveis é fazer "sentiment analysis" dos dados - ou seja, que emoções estão associadas a um texto. A área da análise textual de sentimento está em franca expanção, com diversos recursos e metodologias a ser aplicadas.

Para este trabalho, e por uma questão de limitação de tempo e recursos, recorreu-se a uma das formas de análise mais rudimentares - a análise por associação de sentimento palavra a palavra.

Para tal, recorreu-se ao [trabalho de Saif M. Mohammad e Peter D. Turney](https://saifmohammad.com/WebPages/NRC-Emotion-Lexicon.html) que, através de crowsoursing, criaram uma base de dados com 14.183 palavras que atribui um valor booleano a um ou mais dos oito sentimentos básicos definidos na teoria Robert Plutchik. 

Segundo este autor, as oito emoções básicas são: Raiva (Anger), Medo (Fear), Trizteza (Sadness), Nojo (Disgust), Surpresa (Surprise), Antecipação (Anticipation), Alegria (Joy) e Confiança (Joy).

Assim, em jeito de exemplo, a palavra "abominar" provoca, segundo a base de dados dos autores, os sentimentos de Raiva, Nojo, e Medo, ao passo que a palavra "elogio" provoca sentimentos de Antecipação, Felicidiade e Confiança.

Esta metodologia é problemática na medida que não toma a palavras no seu contexto frásico que, como sabemos, pode variar muito. Além disso, a classificação das palavras foi efetuada em Inglês, tendo os autores procedido depois à sua tradução. Apesar dos autores garantirem que, daquilo que puderam observar, uma grande maioria das classificações feitas "serem estáveis em várias línguas", estamos conscientes de que uma classificação do mesmo género feita por falates de língua portuguesa e a partir de uma lista de palavras nesse mesmo idioma seria mais eficiente. No entanto, não foi encontrada uma base de dados com essas caracterítiscas.

A lista de palavras e a respetiva classificação de Saif M. Mohammad e Peter D. Turney, do National Research Council Canada, pode ser consultada [aqui](input/NRC-Emotion-Lexicon.csv)



```r
sentiments <- import("input/NRC-Emotion-Lexicon.csv")
data <- data %>% left_join(sentiments, by= "word")

data_sentiment <- data %>% na.omit()
```

## Mais palavras positivas ou negativas?


```r
sum(data_sentiment$Positive)
```

```
## [1] 5595
```

```r
(sum(data_sentiment$Positive)/nrow(data_sentiment))*100
```

```
## [1] 22.47981
```

```r
sum(data_sentiment$Negative)
```

```
## [1] 3486
```

```r
(sum(data_sentiment$Negative)/nrow(data_sentiment))*100
```

```
## [1] 14.00619
```
Na globalidade, os textos procuram usar palavras mais positivas do que negativas.


```r
sentimentos_all <- data_sentiment %>% 
                summarise_at(c("Anger","Anticipation","Disgust","Fear","Joy","Sadness","Surprise","Trust"), sum, na.rm = TRUE)

sentimentos_all <- gather(sentimentos_all)
sentimentos_all$per <- sentimentos_all$value/ sum(sentimentos_all$value)
```

# Sentimentos mais comuns por partido


```r
sentimentos_partido <- data_sentiment %>% 
                group_by(partido) %>% 
                summarise_at(c("Anger","Anticipation","Disgust","Fear","Joy","Sadness","Surprise","Trust"), sum, na.rm = TRUE)

sentimentos_partido <- as.data.frame(t(sentimentos_partido))
colnames(sentimentos_partido ) <- as.character(unlist(sentimentos_partido [1,]))
sentimentos_partido <- sentimentos_partido[-1, ]

sentimentos_partido <- sentimentos_partido %>%  add_rownames( var = "sentimento" )
```

```
## Warning: Deprecated, use tibble::rownames_to_column() instead.
```

```r
# 
sentimentos_partido[2:18] <- lapply(sentimentos_partido[2:18], function(x) as.numeric(as.character(x)))

export(sentimentos_partido,"output/sentimentos_partido.csv","csv")
```


# Palavras por sentimento


```r
confianca <- data %>% filter(Trust == 1)

confianca <- confianca %>%
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))

pal <- brewer.pal(9,"Blues")

confianca  %>% 
  with(wordcloud(word, n, random.order = FALSE, max.words = 100, colors=pal))
```

![](main_files/figure-html/unnamed-chunk-13-1.png)<!-- -->


```r
anger <- data %>% filter(Anger == 1)

anger <- anger %>%
                  count(word, sort = TRUE) %>% 
                  mutate(word = reorder(word, n))

anger_esquerda <- data %>% filter(partido == "bloco.pdf.txt" | partido == "cdu.txt" | partido == "ps.txt" ) %>% 
  filter(Anger == 1)



anger_esquerda <- anger_esquerda %>%
                  count(word, sort = TRUE) %>%
                  mutate(word = reorder(word, n))


fear_cds <- data %>% filter(partido == "cds.pdf.txt") %>% 
  filter(Fear == 1)

fear_cds <- fear_cds %>%
                  count(word, sort = TRUE) %>%
                  mutate(word = reorder(word, n))

anticipation_psd <- data %>% filter(partido == "psd.pdf.txt") %>% 
  filter(Anticipation == 1)


anticipation_psd  <- anticipation_psd  %>%
                  count(word, sort = TRUE) %>%
                  mutate(word = reorder(word, n))
```



```r
esquerda <- data %>% filter(partido == "bloco.pdf.txt" | partido == "cdu.txt" | partido == "ps.txt" ) 


direita <- data %>% filter(partido == "psd.pdf.txt" | partido == "cds.txt") 


so_esquerda <- anti_join(esquerda, direita, by = "word") %>% 
                    count(word, sort = TRUE) %>%
                  mutate(word = reorder(word, n))

pal <- brewer.pal(9,"OrRd")

so_esquerda  %>% 
  with(wordcloud(word, n, random.order = FALSE, max.words = 20, colors=pal))
```

![](main_files/figure-html/unnamed-chunk-15-1.png)<!-- -->

```r
so_direita <- anti_join(direita, esquerda, by = "word") %>% 
                    count(word, sort = TRUE) %>%
                  mutate(word = reorder(word, n))

pal <- brewer.pal(9,"PuBu")

so_direita %>% 
  with(wordcloud(word, n, random.order = FALSE, max.words = 20, colors=pal))
```

```
## Warning in wordcloud(word, n, random.order = FALSE, max.words = 20, colors
## = pal): horizonte could not be fit on page. It will not be plotted.
```

```
## Warning in wordcloud(word, n, random.order = FALSE, max.words = 20, colors
## = pal): voluntariado could not be fit on page. It will not be plotted.
```

```
## Warning in wordcloud(word, n, random.order = FALSE, max.words = 20, colors
## = pal): europeísmo could not be fit on page. It will not be plotted.
```

![](main_files/figure-html/unnamed-chunk-15-2.png)<!-- -->


# Qual o programa mais difícil de ler?


```r
df <- df %>%
  mutate(syllables = nsyllable(text),
         sentences = nsentence(text),
         words = ntoken(text, remove_punct = TRUE),
         fk_grade = 0.39*(words/sentences) + 11.8*(syllables/words) - 15.59,
         reading_score = (206.835-84.6*syllables/words-1.015*words/sentences)+42)

#http://www.nilc.icmc.usp.br/nilc/download/Reltec28.pdf

rects <- data.frame(xstart = seq(0,80,20), xend = seq(20,100,20), col = letters[1:5])


plot <- df  %>%  ggplot(aes(reorder(partido, reading_score), reading_score)) +
                  geom_col( fill="steelblue") +
                  geom_text(aes(label= round(reading_score)), hjust=1.6, color="white", size=3.5)+
                  xlab(NULL) +
                  ylab("Facilidade de Leitura") +
                  coord_flip()+
                  theme_minimal()

ggsave(file="output/teste_Flesch-Kincaid.svg", plot=plot, width=8, height=10)
```


# Quanto tempo demoraria para ler todos os programas?


```r
# Um adulto, em inglês, consegue ler em média 265 palavras por minuto
93306/265 
```

```
## [1] 352.0981
```


# Prémio Autocentrado


```r
psd <- data %>% filter(partido == "psd.pdf.txt") %>% count(word, sort = TRUE)
```
















